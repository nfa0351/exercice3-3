# Interfaces Graphiques, Clients lourds et Injection
 ++ Une application fenêtrée avec Swing, des patterns comme MVC, ou encore IOC avec l'injection Spring

## Contexte
* Un premier investisseur s'est intéressé au projet, et il souhaite pouvoir consulter rapidement un prototype sur un appareil quel qu'il soit.
* Nous leur proposons de générer un petit programme s'exécutant sur un de nos PCs pour une démo
* Pour ce faire, nous allons développer un nouveau module, badge-gui, se reposant sur badges-wallet comme dépendance, et y exploiter la technologie Spring...
* Par ailleurs, notre équipe va s'installer dans une ferme d'entreprises où les postes de travail sont tous déjà préconfigurés. Le hic, c'est que l'on a aucun droit pour installer quoi que ce soit, et le seul IDE accessible est IntelliJ en version communautaire. Ceux qui sont en télétravail vont devoir s'aligner sur cet outil en l'installant et en l'utilisant. Ce qui est plutôt plaisant, c'est que cela semble être plutôt efficace, au moins autant qu'éclipse en tout cas...

## Objectifs
* Mises en application:
 - [x] (Exercice 1) Démarrage en 2 temps, affichage rapide de notre liste de métadonnées de Badge, puis customisation fine de la liste ...
    - [x] Commencer avec un tutoriel au choix à afficher simplement la liste des badges (métadonnées), en s'aidant d'une classe d'introspection fournie
    - [x] Implémenter plus sérieusement un modèle de tableau avec des comportements d'affichage des cellules/lignes selon certains critères.   
 - [x] (Exercice 2) Permettre l'affichage d'un badge, lors d'un évènement de double-clic sur une ligne
 - [x] (Exercice 3) Permettre l'ajout d'un badge et la mise à jour visuellement du tableau à l'aide du pattern MVC (Désolé mais Observable/Observer, c'est abandonné depuis Java 9)
 - [ ] (Exercice 4) Appliquer le pattern IOC avec Spring pour injecter le DAO, et appliquer ce pattern également sur le projet badges-wallet 

----

## Fenêtre de Dialogue pour ajouter un Badge: création et réutilisation de composants

 - [ ] Pour ajouter un badge au Wallet, commençons donc:
    - [ ] Par ajouter un nouveau composant du "**Swing UI Designer**", mais cette fois choisissons l'option "**Dialog**".
    - [ ] Disposez-y les éléments nécessaires à la sélection 
       - [ ] du code de série avec un **JTextField**
       - [ ] ainsi que de l'image avec un **JFileChooser**
       - [ ] Mais comment fait-on pour les Dates ? Sur le net on trouve un exemple avec **JXDatePicker** mais si on suis la piste de l'auteur, les liens sont vite morts, alors que faire ? Et bien chercher sur notre site magique "mvnrepository.com" et vous trouverez la dépendance à ajouter, afin d'ensuite avoir ce composant dans la palette...
    - [ ] Donc vous avez suivi ? Le challenge est de s'appuyer sur un composant open-source accessible, et voici (peuve si possible:)   
     -  ![dialog][dialog]
 - [ ] Donc là si tout vas bien nous avons posé la maquette de notre fenêtre de Dialogue. A présent, nous allons passer aux contrôles. Pour permettre la création d'un badge, il faut avoir au minimum :
   - Un Code de série
   - Une date de fin ou une date de début et une date de fin à condition que date_fin > date_début
   - une image au format png, gif, jpg ou jpeg
   - le badge peut être déjà périmé par contre
   - [ ] Donc faire en sorte que le bouton OK ne soit dégrisé que si toutes ces conditions sont remplies.
   - [ ] Des indices se trouvent dans ce Diagramme, alors tâchez de l'appliquer ;):
     
      ```plantuml
            @startuml
      
      title __GUI's Class Diagram__\n

      namespace fr.cnam.foad.nfa035.badges.gui {
      class fr.cnam.foad.nfa035.badges.gui.AddBadgeDialog {
      - buttonCancel : JButton
      - buttonOK : JButton
      - codeSerie : JTextField
      - contentPane : JPanel
      - dateDebut : JXDatePicker
      - dateFin : JXDatePicker
      - fileChooser : JFileChooser
      + AddBadgeDialog()
      + getIcon()
      {static} + main()
      + propertyChange()
      + setCaller()
      + setDao()
      - onCancel()
      - onOK()
      - validateForm()
      }
      }
      
      
      namespace fr.cnam.foad.nfa035.badges.gui {
      class fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI {
      ~ tableList : List<DigitalBadge>
      {static} - RESOURCES_PATH : String
      - button1 : JButton
      - panelBas : JPanel
      - panelHaut : JPanel
      - panelImageContainer : JPanel
      - panelParent : JPanel
      - scrollBas : JScrollPane
      - scrollHaut : JScrollPane
      - table1 : JTable
      + BadgeWalletGUI()
      + getBadge()
      + getIcon()
      {static} + main()
      + setAddedBadge()
      - createUIComponents()
      - loadBadge()
      - setCreatedUIFields()
      }
      }
      
      
      fr.cnam.foad.nfa035.badges.gui.AddBadgeDialog .up.|> java.beans.PropertyChangeListener
      fr.cnam.foad.nfa035.badges.gui.AddBadgeDialog -up-|> javax.swing.JDialog
      fr.cnam.foad.nfa035.badges.gui.AddBadgeDialog o-- fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge : badge
      fr.cnam.foad.nfa035.badges.gui.AddBadgeDialog o-- fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI : caller
      fr.cnam.foad.nfa035.badges.gui.AddBadgeDialog o-- fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO : dao
      fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI o-- fr.cnam.foad.nfa035.badges.gui.AddBadgeDialog : dialog
      fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI o-- fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge : badge
      fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI o-- fr.cnam.foad.nfa035.badges.gui.components.BadgePanel : badgePanel
      fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI o-- fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO : dao
      fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI o-- fr.cnam.foad.nfa035.badges.gui.model.BadgesModel : tableModel
      
      
      right footer
      
      
      PlantUML diagram generated by SketchIt! (https://bitbucket.org/pmesmeur/sketch.it)
      For more information about this tool, please contact philippe.mesmeur@gmail.com
      endfooter
      
      @enduml

      ```
   - [ ] Et vous aurez probablement utilité à prendre ceci:
     ```java
         @Override
         public void propertyChange(PropertyChangeEvent evt) {
            if (validateForm()){
                this.buttonOK.setEnabled(true);
            }
            else{
                this.buttonOK.setEnabled(false);
            }
         }
     ```
  - [ ] Et enfin la dernière épreuve, la mise à jour du tableau et le chargement de la nouvelle image,
    - [ ] Voici donc 3 méthodes qui pourraient vous servir:
     ```java
    /**
     * Commentez-moi
     */
    private void setCreatedUIFields() {
        this.badgePanel = new BadgePanel(badge, dao);
        this.badgePanel.setPreferredSize(new Dimension(256, 256));
        this.panelImageContainer = new JPanel();
        this.panelImageContainer.add(badgePanel);
    }


    /**
     * Commentez-moi
     * @param badge
     */
    public void setAddedBadge(DigitalBadge badge) {
        this.badge = badge;
        tableModel.addBadge(badge);
        tableModel.fireTableDataChanged();
        loadBadge(tableModel.getRowCount()-1);
    }

    /**
     * Commentez-moi
     * @param row
     */
    private void loadBadge(int row){
        panelHaut.removeAll();
        panelHaut.revalidate();

        badge = tableList.get(row);
        setCreatedUIFields();
        table1.setRowSelectionInterval(row, row);
        panelHaut.add(scrollHaut);
        panelImageContainer.setPreferredSize(new Dimension(256, 256));
        scrollHaut.setViewportView(panelImageContainer);

        panelHaut.repaint();
    } 
    ```
  - [ ] Merci pour vos chefs-d'oeuvres... ici celà donne:
      - ![ihm2][ihm2]
      - puis ...
      - ![ihm1][ihm1]

    - [ ] *j'ai commencé par rajouter la dépendance Maven dans le pom :*
    - ![maven][maven]
    - [ ] *voici le rendu pour l'IHM d'ajout :*
    - ![ajout][ajout]
    - [ ] *mais j'ai un message d'erreur lors de l'ajout d'un badge :*
    - ![erreur][erreur]
   
    
 

[dialog]: screenshots/Dialog_Design.png "Fenêtre de Dialogue pour la création d'un nouveau Badge"
[ihm1]: screenshots/Test-IHM-ok.png
[ihm2]: screenshots/Test-IHM.png
[maven]: screenshots/maven.png "POM Maven"
[ajout]: screenshots/ajout.png "Ajout IHM"
[erreur]: screenshots/erreur.png "Ajout erreur"

----
