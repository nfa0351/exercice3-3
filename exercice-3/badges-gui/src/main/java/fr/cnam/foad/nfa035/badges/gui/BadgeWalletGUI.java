package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.gui.components.BadgePanel;
import fr.cnam.foad.nfa035.badges.gui.model.BadgesModel;
import fr.cnam.foad.nfa035.badges.gui.renderer.BadgeSizeCellRenderer;
import fr.cnam.foad.nfa035.badges.gui.renderer.DefaultBadgeCellRenderer;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.List;
import java.util.*;

/**
 * Commentez-moi
 */
public class BadgeWalletGUI {

    private JButton button1;
    private JPanel panelParent;
    private JTable table1;
    private JPanel panelImageContainer;
    private JScrollPane scrollBas;
    private JScrollPane scrollHaut;
    private JPanel panelHaut;
    private JPanel panelBas;
    private BadgePanel badgePanel;

    private DigitalBadge badge;
    private DirectAccessBadgeWalletDAO dao;
    List<DigitalBadge> tableList;

    /**
     * aggrégation vers objet AddBadgeDialog
     */
    AddBadgeDialog dialog;

    BadgesModel tableModel;

    /**
     * appel du Getter du badge (l'image) de DigitalBadge
     * @return le badge (File)
     */
    DigitalBadge getBadge() {
        return badge;
    }

    private static final String RESOURCES_PATH = "badges-gui/src/main/resources/";

    /**
     * fenêtre parente de l'interface
     */
    public BadgeWalletGUI() {

        dialog = new AddBadgeDialog(); // instanciation de la fenêtre fille
        dialog.setCaller(this); // définit parent de dialog
        dialog.setDao(dao); // lien ecriture DirectAccessBadgeWalletDAO

        /**
         * bouton permet ouverture fenêtre fille
         */
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //JOptionPane.showMessageDialog(null, "Bien joué!");
                dialog.pack();
                dialog.setVisible(true);
            }
        });

        //TableModel tableModel = TableModelCreator.createTableModel(DigitalBadge.class, tableList);
        BadgesModel tableModel = new BadgesModel(tableList);
        badgePanel.setPreferredSize(scrollHaut.getPreferredSize());

        // 2. Les Renderers...
        table1.setModel(tableModel);
        table1.getColumnModel().getColumn(4).setCellRenderer(new BadgeSizeCellRenderer());
        table1.setDefaultRenderer(Object.class, new DefaultBadgeCellRenderer());
        // Apparemment, les Objets Number sont traités à part, donc il faut le déclarer explicitement en plus de Object
        table1.setDefaultRenderer(Number.class, new DefaultBadgeCellRenderer());
        // Idem pour les Dates
        table1.setDefaultRenderer(Date.class, new DefaultBadgeCellRenderer());

        // 3. Tri initial
        table1.getRowSorter().toggleSortOrder(0);

        table1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                super.mouseClicked(event);
                if (event.getClickCount() == 2) {
                    int row = ((JTable) event.getComponent()).getSelectedRow();
                    panelHaut.removeAll();
                    panelHaut.revalidate();

                    badge = tableList.get(row);
                    setCreatedUIFields();

                    panelHaut.add(scrollHaut);
                    panelImageContainer.setPreferredSize(new Dimension(256, 256));
                    scrollHaut.setViewportView(panelImageContainer);

                    panelHaut.repaint();
                }
            }
        });


    }

    /**
     * Commentez-moi
     * @param args les arguments
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("My Badge Wallet");
        frame.setVisible(true);
        //JFrame.setDefaultLookAndFeelDecorated(true); // plus joli ?
        BadgeWalletGUI gui = new BadgeWalletGUI();
        frame.setContentPane(gui.panelParent);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setIconImage(gui.getIcon().getImage());

    }

    /**
     * Commentez-moi
     * @return ImageIcon
     */
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/logo.png"));
    }

    /**
     * Commentez-moi
     */
    private void createUIComponents() {
        try {
            this.dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            // 1. Le Model
            Set<DigitalBadge> metaSet = new TreeSet<>(dao.getWalletMetadata());
            this.tableList = new ArrayList<>();
            tableList.addAll(metaSet);
            // Badge initial
            this.badge = tableList.get(0);

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        this.setCreatedUIFields();

    }

    /**
     * permet la création de la partie graphique dynamique
     */
    private void setCreatedUIFields() {
        this.badgePanel = new BadgePanel(badge, dao); //appel contructeur avec 2 param
        this.badgePanel.setPreferredSize(new Dimension(256, 256)); //dimension du badge
        this.panelImageContainer = new JPanel(); //instanciation de PanelImageContainer
        this.panelImageContainer.add(badgePanel); //ajout dynamique de l'image dans PanelImagecontainer
    }

    /**
     * Commentez-moi
     * @param badge
     */
    public void setAddedBadge(DigitalBadge badge) {
        this.badge = badge;
        tableModel.addBadge(badge);
        tableModel.fireTableDataChanged();
        loadBadge(tableModel.getRowCount()-1);
    }

    /**
     * Commentez-moi
     * @param row
     */
    private void loadBadge(int row){
        panelHaut.removeAll();
        panelHaut.revalidate();

        badge = tableList.get(row);
        setCreatedUIFields();
        table1.setRowSelectionInterval(row, row);
        panelHaut.add(scrollHaut);
        panelImageContainer.setPreferredSize(new Dimension(256, 256));
        scrollHaut.setViewportView(panelImageContainer);

        panelHaut.repaint();
    }

    /**
     * lien vers le fomulaire enfant
     * @param dialog
     */
    public void setDialog(AddBadgeDialog dialog) {
        this.dialog = dialog;
    }

}
