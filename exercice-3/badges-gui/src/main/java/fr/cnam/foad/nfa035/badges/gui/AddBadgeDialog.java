package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.jdesktop.swingx.JXDatePicker;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Date;

public class AddBadgeDialog extends JDialog implements PropertyChangeListener {
    private JPanel contentPane;
    private JButton buttonCancel;
    private JButton buttonOK;
    private JTextField codeSerie;
    private JFileChooser fileChooser;
    private JXDatePicker dateDebut;
    private JXDatePicker dateFin;

    /**
     * aggrégation vers autres objets
     */
    BadgeWalletGUI caller;
    DigitalBadge badge;
    DirectAccessBadgeWalletDAO dao;

    /**
     * définit le chemin du fichier icône
     * @return ImageIcon
     */
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/logo.png"));
    }

    public static void main(String[] args) {
        //JFrame frame = new JFrame("Ajouter un badge");
        AddBadgeDialog dialog = new AddBadgeDialog();
        dialog.setContentPane(dialog.contentPane);
        dialog.setDefaultCloseOperation(dialog.EXIT_ON_CLOSE);
        dialog.pack();
        dialog.setIconImage(dialog.getIcon().getImage());
        dialog.setVisible(true);
    }

    /**
     * surveille les changements dans les 4 champs
     */
    public AddBadgeDialog() {
        codeSerie.addPropertyChangeListener(this);
        dateDebut.addPropertyChangeListener(this);
        dateFin.addPropertyChangeListener(this);
        fileChooser.addPropertyChangeListener(this);
        setContentPane(contentPane); //definit la fenêtre principale

        /**
         * action du bouton OK
         */
        buttonOK.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e the event to be processed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    onOK(); //appel méthode onOK
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        /**
         * action du bouton Annuler
         */
        buttonCancel.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e the event to be processed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel(); //appel méthode onCancel
            }
        });
    }

    /**
     * lien vers le formulaire parent
     * @param caller
     */
    public void setCaller(BadgeWalletGUI caller) {
        this.caller = caller;
    }

    /**
     * lien vers le setter de DirectAccessBadgeWalletDAO
     * @param dao
     */
    public void setDao(DirectAccessBadgeWalletDAO dao) {
        this.dao = dao;
    }

    private void onCancel() {
        dispose();
    }

    /**
     * ajoute un badge au wallet
     * @throws IOException
     */
    private void onOK() throws IOException {
        dao.addBadge(badge); //ajout wallet
        dispose();
    }

    /**
     * renvoie vrai si tous les champs sont valides
     * @return
     */
    private boolean validateForm() {

        String code = codeSerie.toString();
        Date debut = dateDebut.getDate();
        Date fin = dateFin.getDate();
        File image = fileChooser.getSelectedFile();
        return true;
    }


    /**
     * This method gets called when a bound property is changed.
     *
     * @param evt A PropertyChangeEvent object describing the event source
     *            and the property that has changed.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (validateForm()){
            this.buttonOK.setEnabled(true);
        }
        else{
            this.buttonOK.setEnabled(false);
        }
    }

}
